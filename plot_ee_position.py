#!/usr/bin/env python

"""

read robot log file and plot ee position  

"""
import numpy as np 
from matplotlib import pyplot as plt 
from tkinter import Tk  
from tkinter.filedialog import askopenfilename
import pandas as pd
import math

from arm_kine_dyna import arm_kinematics

# open csv.file
Tk().withdraw() # 
robot_log_data = askopenfilename() # 
print(robot_log_data)
# read encoder data

robot_info = pd.read_csv(robot_log_data)
motor_pos = []
link_pos = []
for i in range(7):
	motor_pos.append(robot_info['Motor'+str(i)+'Pos'])
	link_pos.append(robot_info['Link'+str(i)+'Pos'])

motor_pos_arr = np.array(motor_pos)
link_pos_arr = np.array(link_pos)

# plot the ee position
fig = plt.figure()
ax = fig.add_subplot(projection = '3d')

# forward kinematics
arm_kine = arm_kinematics()

length = np.size(motor_pos_arr,1)
ee_pos_motor_all = np.zeros((3,length))
ee_pos_link_all = np.zeros((3,length))

for i in range(length):
	q_motor = motor_pos_arr[:,i]
	ee_pos_motor = arm_kine.fkine_gen(q_motor) # ee pos
	ee_pos_motor_all[:,i] = ee_pos_motor*1000 # to mm

	q_link = link_pos_arr[:,i]
	ee_pos_link = arm_kine.fkine_gen(q_link) 
	ee_pos_link_all[:,i] = ee_pos_link*1000 # to mm

ax.plot(ee_pos_motor_all[0,:],ee_pos_motor_all[1,:],ee_pos_motor_all[2,:],color='red')
ax.plot(ee_pos_link_all[0,:],ee_pos_link_all[1,:],ee_pos_link_all[2,:],color='green')
ax.set_xlabel('X (mm)')
ax.set_ylabel('Y (mm)')
ax.set_zlabel('Z (mm)')

plt.legend(['Motor pos', 'Link pos'])
plt.title('EE pos from motor and link encoder')
plt.show()
